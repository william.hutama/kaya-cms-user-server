package com.kaya.cms.user.repository;

import org.springframework.stereotype.Repository;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.entity.AppRoles;

@Repository
public interface AppRolesRepository extends BaseRepository<AppRoles, String> {
	
	AppRoles findAllByLabel(String label);
	AppRoles findAllById(String id);
	
}
