package com.kaya.cms.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.entity.AppRoleMenu;

@Transactional
@Repository
public interface AppRoleMenuRepository extends BaseRepository<AppRoleMenu, Integer> {	

	List<AppRoleMenu> findAllByRoleIdAndCategoryIdAndMenuIsActive(Integer roleId, Integer categoryId, String isActives);

	@Modifying
	@Query(value = "DELETE FROM app_role_menu WHERE arm_ac_id = ?1 AND arm_ar_id = ?2 ", nativeQuery = true)
	int deleteRoleMenuByCategoryIdAndRoleId(Integer categoryId, String roleId);

}
