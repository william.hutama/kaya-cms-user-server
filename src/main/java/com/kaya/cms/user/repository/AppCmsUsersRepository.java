package com.kaya.cms.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.entity.AppCmsUsers;

@Repository
public interface AppCmsUsersRepository extends BaseRepository<AppCmsUsers, Integer> {

	List<AppCmsUsers> findAllByUserIdOrderByIdAsc(String userId);
	List<AppCmsUsers> findAllByCmsUserDraftIdOrderByIdAsc(Integer cmsUserDraftId);
	List<AppCmsUsers> findAllByEmail(String email);
	List<AppCmsUsers> findAllByMobilePhone(String mobilePhone);
	AppCmsUsers findByUserId(String userId);
	AppCmsUsers findByCmsUserDraftId(Integer cmsUserDraftId);

	@Modifying
	@Query(value = "DELETE FROM app_cms_users WHERE acu_email = ?1 ", nativeQuery = true)
	int deleteByEmail(String email);

	@Modifying
	@Query(value = "DELETE FROM app_cms_users WHERE acu_mobile_phone = ?1 ", nativeQuery = true)
	int deleteByMobilePhone(String mobilePhone);

}
