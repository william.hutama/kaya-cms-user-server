package com.kaya.cms.user.repository;

import org.springframework.stereotype.Repository;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.entity.AppMobileUsers;

@Repository
public interface AppMobileUsersRepository extends BaseRepository<AppMobileUsers, Integer> {

}
