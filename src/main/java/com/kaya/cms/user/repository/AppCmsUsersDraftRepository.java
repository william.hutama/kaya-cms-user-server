package com.kaya.cms.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.entity.AppCmsUsersDraft;

@Repository
public interface AppCmsUsersDraftRepository extends BaseRepository<AppCmsUsersDraft, Integer> {

	List<AppCmsUsersDraft> findAllByMobilePhoneOrderByIdAsc(String mobilePhone);

	@Modifying
	@Query(value = "DELETE FROM app_cms_users_draft WHERE acud_email = ?1 ", nativeQuery = true)
	int deleteByEmailDraft(String email);

	@Modifying
	@Query(value = "DELETE FROM app_cms_users_draft WHERE acud_mobile_phone = ?1 ", nativeQuery = true)
	int deleteByMobilePhoneDraft(String mobilePhone);

}
