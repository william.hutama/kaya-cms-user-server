package com.kaya.cms.user.repository;

import org.springframework.stereotype.Repository;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.entity.AppCategories;

@Repository
public interface AppCategoriesRepository extends BaseRepository<AppCategories, Integer> {
}
