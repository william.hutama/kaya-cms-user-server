package com.kaya.cms.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.entity.AppMenus;

@Repository
public interface AppMenusRepository extends BaseRepository<AppMenus, Integer> {

	Page<AppMenus> findAllByCategoryType(String categoryType, Pageable pageable);
	Page<AppMenus> findAllByCategoryTypeAndIsActive(String categoryType, String isActive, Pageable pageable);

}
