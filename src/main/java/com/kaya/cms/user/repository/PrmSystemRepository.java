package com.kaya.cms.user.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.entity.PrmSystem;

@Repository
public interface PrmSystemRepository extends BaseRepository<PrmSystem, Integer> {
	
	List<PrmSystem> findAllByDescription(String description); 

}
