package com.kaya.cms.user.util;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.regex.Pattern;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class ObjectSpecificationUtil<T> implements Specification<T> {

	private static final long serialVersionUID = 1L;
	private Class<T> entityClass;
	private String propertyName;
	private String propertyCondition;
	private String propertyValue;
	private Object objectValue;
	private Object objectValue2;


	public ObjectSpecificationUtil(Class<T> entityClass, String propertyName, String propertyValue) {
		this.entityClass = entityClass;
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
	}


	public ObjectSpecificationUtil(Class<T> entityClass, String propertyName, String propertyCondition, Object objectValue) {
		this.entityClass = entityClass;
		this.propertyName = propertyName;
		this.propertyCondition = propertyCondition;
		this.objectValue = objectValue;
	}


	public ObjectSpecificationUtil(Class<T> entityClass, String propertyName, String propertyCondition, Object objectValue, Object objectValue2) {
		this.entityClass = entityClass;
		this.propertyName = propertyName;
		this.propertyCondition = propertyCondition;
		this.objectValue = objectValue;
		this.objectValue2 = objectValue2;
	}


    @Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (propertyCondition == null && propertyValue == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true)); // always true = no filtering
        } else if (propertyCondition != null && objectValue == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true)); // always true = no filtering
        }

		for (Field f : entityClass.getDeclaredFields()) {
	        if (!propertyName.contains(".") && propertyName.equalsIgnoreCase(f.getName())) {
	        	if (propertyCondition!=null) {
	        		if ("is null".equalsIgnoreCase(propertyCondition)) {
	        			return criteriaBuilder.isNull(root.get(propertyName));
	        		} else if ("is not null".equalsIgnoreCase(propertyCondition)) {
	        			return criteriaBuilder.isNotNull(root.get(propertyName));
	        		} else if (f.getType().isPrimitive() || "java.lang.Number".equals(f.getType().getSuperclass().getName())) {
	        			if (Pattern.compile("-?\\d+").matcher(objectValue.toString()).matches()) {
		        			switch (propertyCondition.toLowerCase()) {
					        	case "=" 	: return criteriaBuilder.equal(root.get(propertyName), objectValue);
					        	case ">" 	: return criteriaBuilder.greaterThan(root.get(propertyName), objectValue.toString());
					        	case ">=" 	: return criteriaBuilder.ge(root.get(propertyName), (Number) objectValue);
					        	case "<" 	: return criteriaBuilder.lessThan(root.get(propertyName), objectValue.toString());
					        	case "<=" 	: return criteriaBuilder.le(root.get(propertyName), (Number) objectValue);
					        	default 	: return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
		        			}
	        			} else {
	        				return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
	        			}
					} else if ("java.lang.String".equals(f.getType().getName())) {
	        			switch (propertyCondition.toLowerCase()) {
				        	case "=" 	: return criteriaBuilder.equal(root.get(propertyName), objectValue);
				        	case "like" : return criteriaBuilder.like(root.get(propertyName), objectValue.toString());
				        	default 	: return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
	        			}
					} else if ("java.util.Date".equals(f.getType().getName())) {
	        			switch (propertyCondition.toLowerCase()) {
				        	case "="		: return criteriaBuilder.equal(root.get(propertyName), (Date) objectValue);
				        	case ">"		: return criteriaBuilder.greaterThan(root.get(propertyName), (Date) objectValue);
				        	case "<" 		: return criteriaBuilder.lessThan(root.get(propertyName), (Date) objectValue);
				        	case "between"	: return criteriaBuilder.between(root.get(propertyName), (Date) objectValue, (Date) objectValue2);
				        	default			: return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
	        			}
					} else {
						return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
					}
	        	} else {
					if (f.getType().isPrimitive()) {
						if (Pattern.compile("-?\\d+").matcher(propertyValue.toString()).matches()) {
			            	return criteriaBuilder.equal(root.get(propertyName), propertyValue);
						} else {
							return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
						}
					} else if ("java.lang.Number".equals(f.getType().getSuperclass().getName())) {
						if (Pattern.compile("-?\\d+").matcher(propertyValue.toString()).matches()) {
			            	return criteriaBuilder.equal(root.get(propertyName), propertyValue);
						} else {
							return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
						}
					} else if ("java.lang.String".equals(f.getType().getName())) {
		            	return criteriaBuilder.like(criteriaBuilder.lower(root.get(propertyName)), "%"+propertyValue.toLowerCase()+"%");
					} else if ("java.util.Date".equals(f.getType().getName())) {
						return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
					} else {
		            	return criteriaBuilder.equal(root.get(propertyName), propertyValue);
					}
	        	}
	        } else if (propertyName.contains(".") && propertyName.split("\\.")[0].equalsIgnoreCase(f.getName())) {
				if (root.get(propertyName.split("\\.")[0]).get(propertyName.split("\\.")[1]).getJavaType().isPrimitive()) {
					if (Pattern.compile("-?\\d+").matcher(propertyValue.toString()).matches()) {
		            	return criteriaBuilder.equal(root.get(propertyName.split("\\.")[0]).get(propertyName.split("\\.")[1]), propertyValue);
					} else {
						return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
					}
				} else if ("java.lang.Number".equals(root.get(propertyName.split("\\.")[0]).get(propertyName.split("\\.")[1]).getJavaType().getSuperclass().getName())) {
					if (Pattern.compile("-?\\d+").matcher(propertyValue.toString()).matches()) {
		            	return criteriaBuilder.equal(root.get(propertyName.split("\\.")[0]).get(propertyName.split("\\.")[1]), propertyValue);
					} else {
						return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
					}
				} else if ("java.lang.String".equals(root.get(propertyName.split("\\.")[0]).get(propertyName.split("\\.")[1]).getJavaType().getName())) {
	            	return criteriaBuilder.like(criteriaBuilder.lower(root.get(propertyName.split("\\.")[0]).get(propertyName.split("\\.")[1])), "%"+propertyValue.toLowerCase()+"%");
				} else if ("java.util.Date".equals(root.get(propertyName.split("\\.")[0]).get(propertyName.split("\\.")[1]).getJavaType().getName())) {
					return criteriaBuilder.isTrue(criteriaBuilder.literal(false));
				} else {
	            	return criteriaBuilder.equal(root.get(propertyName.split("\\.")[0]).get(propertyName.split("\\.")[1]), propertyValue);
				}
	        }
		}
        return criteriaBuilder.isTrue(criteriaBuilder.literal(true)); // always true = no filtering
	}
}
