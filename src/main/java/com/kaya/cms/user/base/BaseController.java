package com.kaya.cms.user.base;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.kaya.cms.user.base.dto.ResponsePage;
import com.kaya.cms.user.base.dto.ResponseState;
import com.kaya.cms.user.util.ObjectMapperUtil;

public final class BaseController {

	private BaseController() {
	}

	public static final ResponseState getResponseState(HttpServletRequest request, HttpStatus status, String error, String requestId) {
	    ResponseState responseState = new ResponseState(status);
	    responseState.setTimestamp(new Date());
	    responseState.setRequestId(requestId);
	    responseState.setStatus(status.value());
	    responseState.setError(error);
	    responseState.setMessage(status.value()==HttpStatus.OK.value()?status.name():error);
	    responseState.setPath(request.getRequestURI().toString() + (request.getQueryString()!=null ? "?" + request.getQueryString() : ""));
	    return responseState;
	}

	public static final <T> ResponsePage getResponsePage(Page<T> data) {
	    ResponsePage responsePage = new ResponsePage();
	    responsePage.setPageNumber(data.getNumber());
	    responsePage.setPageSize(data.getSize());
	    responsePage.setTotalElements(data.getTotalElements());
	    responsePage.setTotalPages(data.getTotalPages());
	    responsePage.setFirst(data.isFirst());
	    responsePage.setLast(data.isLast());
	    responsePage.setEmpty(data.isEmpty());
	    return responsePage;
	}

	public static final <T, D> ResponseEntity<Map<String,Object>> getResponseEntityPage(Page<T> data, Class<D> dtoClass, HttpServletRequest request, HttpStatus status, String error, String requestId) {
	    Map<String, Object> response = new HashMap<String, Object>();
	    response.put("data", ObjectMapperUtil.mapAll(data.getContent(), dtoClass));
	    response.put("page", getResponsePage(data));
	    response.put("state", getResponseState(request, status, error, requestId));
	    return new ResponseEntity<Map<String,Object>>(response, status);
	}


	public static final <T, D> ResponseEntity<Map<String,Object>> getResponseEntityList(List<T> data, Class<D> dtoClass, HttpServletRequest request, HttpStatus status, String error, String requestId) {
	    Map<String, Object> response = new HashMap<String, Object>();
	    if (data!=null) {
		    response.put("data", ObjectMapperUtil.mapAll(data, dtoClass));
	    }
	    response.put("state", getResponseState(request, status, error, requestId));
	    return new ResponseEntity<Map<String,Object>>(response, status);
	}


	public static final <T, D> ResponseEntity<Map<String,Object>> getResponseEntity(T data, Class<D> dtoClass, HttpServletRequest request, HttpStatus status, String error, String requestId) {
	    Map<String, Object> response = new HashMap<String, Object>();
	    if (data!=null) {
		    response.put("data", ObjectMapperUtil.map(data, dtoClass));
	    }
	    response.put("state", getResponseState(request, status, error, requestId));
	    return new ResponseEntity<Map<String,Object>>(response, status);
	}

	public static final <T, D> ResponseEntity<Map<String,Object>> getResponseEntity(T data, HttpServletRequest request, HttpStatus status, String error, String requestId) {
	    Map<String, Object> response = new HashMap<String, Object>();
	    if (data!=null) {
		    response.put("data", data);
	    }
	    response.put("state", getResponseState(request, status, error, requestId));
	    return new ResponseEntity<Map<String,Object>>(response, status);
	}
}
