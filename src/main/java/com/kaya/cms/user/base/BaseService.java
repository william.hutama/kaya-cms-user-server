package com.kaya.cms.user.base;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.kaya.cms.user.common.CommonRepository;
import com.kaya.cms.user.util.ObjectSpecificationUtil;

@Service
@Transactional
public abstract class BaseService<T, ID extends Serializable> extends CommonRepository {

	@SuppressWarnings("unchecked")
	private final Class<T> entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	public abstract BaseRepository<T, ID> getBaseRepository();

	public Specification<T> buildSpec(Map<String, String> searchParams) {

		Specification<T> spec = null;
		Specification<T> searchValueSpec = null;

		if (searchParams!=null && !searchParams.isEmpty()) {
			
			// exclude paging parameter
			searchParams.remove("size");
			searchParams.remove("page");
			searchParams.remove("sort");

			int keyIdx = 0;
			String value = null;
			for (String key : searchParams.keySet()) {
				value = searchParams.get(key).replaceAll("%20", " ");
				if (!"searchValue".equals(key) && !"searchKey".equals(key)) {
					if (keyIdx==0) {
						spec = Specification.where(new ObjectSpecificationUtil<T>(entityClass, key, value));
					} else {
						spec = spec.and(new ObjectSpecificationUtil<T>(entityClass, key, value));
					}
					keyIdx++;
				}
			}
			
			// add filter by searchValue
			int keySearchValueIdx= 0;
			String searchValue = null;
			if (searchParams.containsKey("searchValue")) {
				searchValue = searchParams.get("searchValue").replaceAll("%20", " ");
				if (!"".equals(searchValue.trim())) {
					if (searchParams.containsKey("searchKey")) {
						String searchParam = searchParams.get("searchKey").replaceAll("%20", " ").trim();
						if (searchParam.contains(",")) {
							String[] searchKeys = searchParam.split(",");
							for (String searchKey : searchKeys) {
								if (keySearchValueIdx==0) {
									searchValueSpec = new ObjectSpecificationUtil<T>(entityClass, searchKey.trim(), searchValue);
								} else {
									searchValueSpec = searchValueSpec.or(new ObjectSpecificationUtil<T>(entityClass, searchKey.trim(), searchValue));
								}
								keySearchValueIdx++;
							}
						} else {
							searchValueSpec = new ObjectSpecificationUtil<T>(entityClass, searchParam, searchValue);
						}
					} else {
						for (Field f : entityClass.getDeclaredFields()) {
							if (!f.getType().isPrimitive() && ("java.lang.String".equals(f.getType().getName())) && !"PREFIX".equalsIgnoreCase(f.getName())) {
								if (keySearchValueIdx==0) {
									searchValueSpec = new ObjectSpecificationUtil<T>(entityClass, f.getName(), searchValue);
								} else {
									searchValueSpec = searchValueSpec.or(new ObjectSpecificationUtil<T>(entityClass, f.getName(), searchValue));
								}
								keySearchValueIdx++;
							}
						}
					}
					if (spec==null) {
						spec = Specification.where(searchValueSpec);
					} else {
						spec = spec.and(searchValueSpec);
					}
				}
			}
		}

		return spec;
	}

	public Page<T> findAll(Map<String, String> searchParams, Pageable pageable) {
		return getBaseRepository().findAll(buildSpec(searchParams), pageable);
	}

	public T findById(ID id) {
		Optional<T> opt = getBaseRepository().findById(id);
		if (opt.isPresent()) {
			return opt.get();
		}
		return null;
	}

	public List<T> findAllById(List<ID> id) {
		return getBaseRepository().findAllById(id);
	}

	public Page<T> findPage(Map<String, String> searchParams, Pageable pageable) {
		return getBaseRepository().findAll(buildSpec(searchParams), pageable);
	}

	public T insert(T entity) {
		return getBaseRepository().save(entity);
	}

	public List<T> insertAll(List<T> entity) {
		return getBaseRepository().saveAll(entity);
	}

	public T update(T entity) {
		return getBaseRepository().save(entity);
	}

	public List<T> updateAll(List<T> entity) {
		return getBaseRepository().saveAll(entity);
	}

	public void delete(T entity) {
		getBaseRepository().delete(entity);
	}

	public void deleteAll(List<T> entity) {
		getBaseRepository().deleteAll(entity);
	}

}
