package com.kaya.cms.user.base;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.kaya.cms.user.util.ObjectMapperUtil;

public abstract class BaseControllerHelper<T, D, ID extends Serializable> {

	@SuppressWarnings("unchecked")
	private final Class<T> entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	@SuppressWarnings("unchecked")
	private final Class<T> dtoClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];

	public abstract BaseService<T, ID> getBaseService();

	@GetMapping("/all/{idList}")
	@ResponseBody
	public HttpEntity<Map<String,Object>> getAll(@RequestHeader(value = "requestId", required = false) String requestId,
												 @PathVariable("idList") List<ID> idList,
												 HttpServletRequest request) {
        List<T> data = getBaseService().findAllById(idList);
        return BaseController.getResponseEntityList(data, dtoClass, request, HttpStatus.OK, "", requestId);
	}

	@GetMapping("/page")
	@ResponseBody
	public HttpEntity<Map<String,Object>> getPage(@RequestHeader(value = "requestId", required = false) String requestId,
											      @RequestParam Map<String, String> searchParams,
											      Pageable pageable,
											      HttpServletRequest request) {
        Page<T> data = getBaseService().findPage(searchParams, pageable);
        return BaseController.getResponseEntityPage(data, dtoClass, request, HttpStatus.OK, "", requestId);
	}

	@GetMapping("/{id}")
	@ResponseBody
	public HttpEntity<Map<String,Object>> get(@RequestHeader(value = "requestId", required = false) String requestId,
											  @PathVariable("id") ID id,
											  HttpServletRequest request) {
        T data = getBaseService().findById(id);
        return BaseController.getResponseEntity(data, dtoClass, request, HttpStatus.OK, "", requestId);
	}

	@PostMapping
	@ResponseBody
	public HttpEntity<Map<String,Object>> insert(@RequestHeader(value = "requestId", required = false) String requestId,
												 @RequestBody D dto,
												 HttpServletRequest request) {
		T data = getBaseService().insert(ObjectMapperUtil.map(dto, entityClass));
        return BaseController.getResponseEntity(data, dtoClass, request, HttpStatus.CREATED, "", requestId);
	}

	@PostMapping("/all")
	@ResponseBody
	public HttpEntity<Map<String,Object>> insertAll(@RequestHeader(value = "requestId", required = false) String requestId,
													@RequestBody List<D> dto,
													HttpServletRequest request) {
		if (dto!=null && dto.size()>0) {
        	List<T> data = getBaseService().insertAll(ObjectMapperUtil.mapAll(dto, entityClass));
	        return BaseController.getResponseEntity(data, dtoClass, request, HttpStatus.CREATED, "", requestId);
		}
		else return null;
	}

	@PostMapping("/update")
	@ResponseBody
	public HttpEntity<Map<String,Object>> updatePost(@RequestHeader(value = "requestId", required = false) String requestId,
												     @RequestBody D dto,
												     HttpServletRequest request) {
		T data = getBaseService().update(ObjectMapperUtil.map(dto, entityClass));
		return BaseController.getResponseEntity(data, dtoClass, request, HttpStatus.OK, "", requestId);
	}

	@PostMapping("/delete/{id}")
	@ResponseBody
	public HttpEntity<Map<String,Object>> deletePost(@RequestHeader(value = "requestId", required = false) String requestId,
			  										 @PathVariable("id") ID id,
												     HttpServletRequest request) {
        T data = getBaseService().findById(id);
        if (data==null) {
            return BaseController.getResponseEntity(null, dtoClass, request, HttpStatus.CONFLICT, "Data is not found", requestId);
        }
		getBaseService().delete(data);
		return BaseController.getResponseEntity(null, dtoClass, request, HttpStatus.OK, "", requestId);
	}

	@PutMapping
	@ResponseBody
	public HttpEntity<Map<String,Object>> update(@RequestHeader(value = "requestId", required = false) String requestId,
												 @RequestBody D dto,
												 HttpServletRequest request) {
		T data = getBaseService().update(ObjectMapperUtil.map(dto, entityClass));
		return BaseController.getResponseEntity(data, dtoClass, request, HttpStatus.OK, "", requestId);
	}

	@PutMapping("/all")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public HttpEntity<Map<String,Object>> updateAll(@RequestHeader(value = "requestId", required = false) String requestId,
													@RequestBody List<D> dto,
													HttpServletRequest request) {
		if (dto!=null && dto.size()>0) {
			List<T> data = getBaseService().updateAll(ObjectMapperUtil.mapAll(dto, entityClass));
	        return BaseController.getResponseEntity(data, dtoClass, request, HttpStatus.OK, "", requestId);
		}
		else return null;
	}

	@DeleteMapping("/all/{idList}")
	@ResponseBody
	public HttpEntity<Map<String,Object>> deleteAll(@RequestHeader(value = "requestId", required = false) String requestId,
													@PathVariable("idList") List<ID> idList,
													HttpServletRequest request) {
        List<T> data = getBaseService().findAllById(idList);
        if (data==null || data.size()==0) {
            return BaseController.getResponseEntity(null, dtoClass, request, HttpStatus.CONFLICT, "Data is not found", requestId);
        }
		getBaseService().deleteAll(data);
		return BaseController.getResponseEntity(null, dtoClass, request, HttpStatus.OK, "", requestId);
	}

	@DeleteMapping("/{id}")
	@ResponseBody
	public HttpEntity<Map<String,Object>> delete(@RequestHeader(value = "requestId", required = false) String requestId,
												 @PathVariable("id") ID id,
												 HttpServletRequest request) {
        T data = getBaseService().findById(id);
        if (data==null) {
            return BaseController.getResponseEntity(null, dtoClass, request, HttpStatus.CONFLICT, "Data is not found", requestId);
        }
		getBaseService().delete(data);
		return BaseController.getResponseEntity(null, dtoClass, request, HttpStatus.OK, "", requestId);
	}

}
