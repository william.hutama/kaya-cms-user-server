package com.kaya.cms.user.base.dto;

import lombok.Data;

@Data
public class ResponsePage {

	private Integer pageNumber;
	private Integer pageSize;
	private Long totalElements;
	private Integer totalPages;
	private Boolean first;
	private Boolean last;
	private Boolean empty;

}
