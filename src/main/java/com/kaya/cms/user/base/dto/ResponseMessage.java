package com.kaya.cms.user.base.dto;

import lombok.Data;

@Data
public class ResponseMessage {

	private String indonesian;
	private String english;

}
