package com.kaya.cms.user.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.base.BaseService;
import com.kaya.cms.user.entity.AppMobileUsers;

@Service
@Transactional
public class AppMobileUsersService extends BaseService<AppMobileUsers, Integer> {

	@Override
	public BaseRepository<AppMobileUsers, Integer> getBaseRepository() {
		return appMobileUsersRepository;
	}
}
