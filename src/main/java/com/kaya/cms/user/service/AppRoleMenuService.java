package com.kaya.cms.user.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.base.BaseService;
import com.kaya.cms.user.common.CommonConstant.ENTITY_STATUS;
import com.kaya.cms.user.entity.AppRoleMenu;

@Service
@Transactional
public class AppRoleMenuService extends BaseService<AppRoleMenu, Integer> {

	@Override
	public BaseRepository<AppRoleMenu, Integer> getBaseRepository() {
		return appRoleMenuRepository;
	}

	public List<AppRoleMenu> findAllByRoleIdAndCategoryIdAndMenuIsActive(Integer roleId, Integer categoryId) {
		return appRoleMenuRepository.findAllByRoleIdAndCategoryIdAndMenuIsActive(roleId, categoryId, ENTITY_STATUS.YES);
	}

	public String updateRoleMenu(Integer categoryId, String roleId, String selectedMenuId) {
		return null;
	}
	
}
