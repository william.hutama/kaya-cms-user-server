package com.kaya.cms.user.service;

import javax.transaction.Transactional;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.base.BaseService;
import com.kaya.cms.user.entity.AppCategories;


@Transactional
public class AppCategoriesService extends BaseService<AppCategories, Integer> {

	@Override
	public BaseRepository<AppCategories, Integer> getBaseRepository() {
		// TODO Auto-generated method stub
		return appCategoriesRepository;
	}
	
}
