package com.kaya.cms.user.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.kaya.cms.user.common.CommonConstant.APP_PROPERTY;
import com.kaya.cms.user.config.KeycloakProvider;
import com.kaya.cms.user.dto.login.RegisterRequest;

@Service
public class KeycloakAdminClientService {

	@Value("${" + APP_PROPERTY.KEYCLOAK_REALM_PROPERTY + "}")
	private String realm;

	@Value("${" + APP_PROPERTY.KEYCLOAK_RESOURCE_PROPERTY + "}")
	private String clientID;

	@Value("${" + APP_PROPERTY.KAYA_KEYCLOAK_CLIENT_JOIN_ROLE_PROPERTY + "}")
	private String joinRole;

	@Value("${" + APP_PROPERTY.KAYA_KEYCLOAK_CLIENT_ADMINISTRATOR_USERNAME_PROPERTY + "}")
	private String adminUsername;

	@Value("${" + APP_PROPERTY.KAYA_KEYCLOAK_CLIENT_ADMINISTRATOR_PASSWORD_PROPERTY + "}")
	private String adminPassword;

	@Autowired
    private KeycloakProvider kcProvider;

    public HashMap<String, Object> newToken(String username, String password) {
    	return kcProvider.newToken(username, password);
    }

    public HashMap<String, Object> refreshToken(String refreshToken) {
    	return kcProvider.refreshToken(refreshToken);
    }

    public String createKeycloakUser(RegisterRequest request) {
        UsersResource usersResource = kcProvider.getInstance(adminUsername, adminPassword).realm(realm).users();
        ClientsResource clientsResource = kcProvider.getInstance(adminUsername, adminPassword).realm(realm).clients();
        
        CredentialRepresentation credentialRepresentation = createPasswordCredentials(request.getPassword());

        UserRepresentation kcUser = new UserRepresentation();
        if (request!=null) {
            kcUser.setUsername(request.getEmail());
            kcUser.setFirstName(request.getFirstName());
            kcUser.setLastName(request.getLastName());
            kcUser.setEmail(request.getEmail());
            kcUser.setEnabled(true);
            kcUser.setEmailVerified(false);
        }

        Response response = usersResource.create(kcUser);

        if (response.getStatus() == 201) { //if Conflict == 409
        	String userId = response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
        	usersResource.get(userId).resetPassword(credentialRepresentation);

        	ClientRepresentation clientRepresentation = clientsResource.findByClientId(clientID).get(0);
        	RoleRepresentation clientRoleRepresentation = clientsResource.get(clientRepresentation.getId()).roles().get(joinRole).toRepresentation();
        	usersResource.get(userId).roles().clientLevel(clientRepresentation.getId()).add(Arrays.asList(clientRoleRepresentation));

        	return userId;
        }

        return null;
    }

    public String getKeycloakUser(String username, String password) {
        UsersResource usersResource = kcProvider.getInstance(adminUsername, adminPassword).realm(realm).users();

        List<UserRepresentation> userList = usersResource.search(username);
        if (userList!=null && userList.size()>0) {
            UserRepresentation user = userList.get(0);

            return user.getId();
        }

        return null;
    }

    public String removeKeycloakUser(String username) {
        UsersResource usersResource = kcProvider.getInstance(adminUsername, adminPassword).realm(realm).users();

        List<UserRepresentation> userList = usersResource.search(username);
        if (userList!=null && userList.size()>0) {
            UserRepresentation user = userList.get(0);
            usersResource.get(user.getId()).remove();
            return user.getId();
        }

        return null;
    }

    public String updatePasswordCredentialsDraft(String username, String newPassword) {
    	if (username!=null && !"".equals(username.trim()))  {
            UsersResource usersResource = kcProvider.getInstance(adminUsername, adminPassword).realm(realm).users();

            List<UserRepresentation> userList = usersResource.search(username);
            if (userList!=null && userList.size()>0) {
                UserRepresentation user = userList.get(0);
                Map<String, List<String>> attributes = user.getAttributes() != null ? user.getAttributes() : new HashMap<String, List<String>>();
                attributes.put("password_temp", Arrays.asList(newPassword));
                user.setAttributes(attributes);
                usersResource.get(user.getId()).update(user);

                return user.getId();
            }
    	}

        return null;
    }

    public String updatePasswordCredentialsFinal(String userId) {
    	if (userId!=null && !"".equals(userId.trim()))  {
            UsersResource usersResource = kcProvider.getInstance(adminUsername, adminPassword).realm(realm).users();

            if (usersResource.get(userId)!=null && usersResource.get(userId).toRepresentation()!=null) {
                UserRepresentation user = usersResource.get(userId).toRepresentation();

                Map<String, List<String>> attributes = user.getAttributes();
                if (attributes!=null && attributes.containsKey("password_temp")) {
                	if (attributes.get("password_temp")!=null && attributes.get("password_temp").size()>0) {
                        CredentialRepresentation credentialRepresentation = createPasswordCredentials(attributes.get("password_temp").get(0));
                    	usersResource.get(user.getId()).resetPassword(credentialRepresentation);

                    	attributes.remove("password_temp");
                        usersResource.get(user.getId()).update(user);

                        return user.getId();
                	}
                }
            }
    	}

        return null;
    }

    private static CredentialRepresentation createPasswordCredentials(String password) {
        CredentialRepresentation passwordCredentials = new CredentialRepresentation();
        passwordCredentials.setTemporary(false);
        passwordCredentials.setType(CredentialRepresentation.PASSWORD);
        passwordCredentials.setValue(password);

        return passwordCredentials;
    }

}
