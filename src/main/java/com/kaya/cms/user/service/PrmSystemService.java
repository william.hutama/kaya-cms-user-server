package com.kaya.cms.user.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.base.BaseService;
import com.kaya.cms.user.entity.PrmSystem;

@Service
@Transactional
public class PrmSystemService extends BaseService<PrmSystem, Integer> {
	
	@Override
	public BaseRepository<PrmSystem, Integer> getBaseRepository() {
		return prmSystemRepository;
	};
	
}
