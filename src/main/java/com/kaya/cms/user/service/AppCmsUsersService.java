package com.kaya.cms.user.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.base.BaseService;
import com.kaya.cms.user.entity.AppCmsUsers;

@Service
@Transactional
public class AppCmsUsersService extends BaseService<AppCmsUsers, Integer> {

	@Override
	public BaseRepository<AppCmsUsers, Integer> getBaseRepository() {
		return appCmsUsersRepository;
	}

	public AppCmsUsers getProfile(String userId) {
		AppCmsUsers appCmsUsers = appCmsUsersRepository.findByUserId(userId);
		
		return appCmsUsers;
	}

	public List<AppCmsUsers> findAllByMobilePhone(String mobilePhone) {
		List<AppCmsUsers> amuList = appCmsUsersRepository.findAllByMobilePhone(mobilePhone);
		if (amuList!=null && amuList.size()>0) {
			return amuList;
		}

		return null;
	}

	public void deleteByEmail(String email) {
		appCmsUsersDraftRepository.deleteByEmailDraft(email);

		appCmsUsersRepository.deleteByEmail(email);
	}

	public void deleteByMobilePhone(String mobilePhone) {
		appCmsUsersDraftRepository.deleteByMobilePhoneDraft(mobilePhone);

		appCmsUsersRepository.deleteByMobilePhone(mobilePhone);
	}

}
