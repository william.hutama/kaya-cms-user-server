package com.kaya.cms.user.service;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.base.BaseService;
import com.kaya.cms.user.entity.AppMenus;

@Service
@Transactional
public class AppMenusService extends BaseService<AppMenus, Integer> {

	@Override
	public BaseRepository<AppMenus, Integer> getBaseRepository() {
		return appMenusRepository;
	};

	public Page<AppMenus> findAllByCategoryType(String categoryType, Pageable pageable){
		return appMenusRepository.findAllByCategoryType(categoryType, pageable);
	}

	public Page<AppMenus> findAllByCategoryTypeAndIsActive(String categoryType, String isActive, Pageable pageable){
		return appMenusRepository.findAllByCategoryTypeAndIsActive(categoryType, isActive, pageable);
	}

}
