package com.kaya.cms.user.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.kaya.cms.user.base.BaseRepository;
import com.kaya.cms.user.base.BaseService;
import com.kaya.cms.user.entity.AppRoles;

@Service
@Transactional
public class AppRolesService extends BaseService<AppRoles, String> {

	@Override
	public BaseRepository<AppRoles, String> getBaseRepository() {
		return appRolesRepository;
	}

}
