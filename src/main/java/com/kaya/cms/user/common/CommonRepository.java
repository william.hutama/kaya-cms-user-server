package com.kaya.cms.user.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kaya.cms.user.repository.AppCategoriesRepository;
import com.kaya.cms.user.repository.AppCmsUsersDraftRepository;
import com.kaya.cms.user.repository.AppCmsUsersRepository;
import com.kaya.cms.user.repository.AppMenusRepository;
import com.kaya.cms.user.repository.AppMobileUsersRepository;
import com.kaya.cms.user.repository.AppRoleMenuRepository;
import com.kaya.cms.user.repository.AppRolesRepository;
import com.kaya.cms.user.repository.PrmSystemRepository;

@Component
public class CommonRepository {

	@Autowired
    protected AppCategoriesRepository appCategoriesRepository;

	@Autowired
    protected AppCmsUsersDraftRepository appCmsUsersDraftRepository;
	
	@Autowired
    protected AppMobileUsersRepository appMobileUsersRepository;
	
	@Autowired
    protected AppCmsUsersRepository appCmsUsersRepository;

	@Autowired
    protected AppMenusRepository appMenusRepository;

	@Autowired
    protected AppRoleMenuRepository appRoleMenuRepository;

	@Autowired
    protected AppRolesRepository appRolesRepository;

	@Autowired
    protected PrmSystemRepository prmSystemRepository;

}
