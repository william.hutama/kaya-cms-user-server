package com.kaya.cms.user.common;

public interface CommonConstant {

	public static final class APP_PROPERTY {
		public static final String SPRING_MAIL_USE_API_PROPERTY			= "spring.mail.useApi";
		public static final String SPRING_MAIL_ENDPOINT_API_PROPERTY	= "spring.mail.endpointApi";
		public static final String SPRING_MAIL_USERNAME_PROPERTY		= "spring.mail.username";
		public static final String SPRING_MAIL_PASSWORD_PROPERTY		= "spring.mail.password";
		public static final String SPRING_OTP_ENDPOINT_API_PROPERTY		= "spring.otp.endpointApi";

		public static final String KEYCLOAK_AUTH_SERVER_URL_PROPERTY = "keycloak.auth-server-url";
		public static final String KEYCLOAK_REALM_PROPERTY = "keycloak.realm";
		public static final String KEYCLOAK_RESOURCE_PROPERTY = "keycloak.resource";
		public static final String KEYCLOAK_CREDENTIALS_SECRET_PROPERTY = "keycloak.credentials.secret";

		public static final String KAYA_KEYCLOAK_CLIENT_JOIN_ROLE_PROPERTY = "kaya-keycloak.kaya-cms-api-client.join-role";
		public static final String KAYA_KEYCLOAK_CLIENT_ADMINISTRATOR_USERNAME_PROPERTY = "kaya-keycloak.kaya-cms-api-client.administrator.username";
		public static final String KAYA_KEYCLOAK_CLIENT_ADMINISTRATOR_PASSWORD_PROPERTY = "kaya-keycloak.kaya-cms-api-client.administrator.password";

	}

	public static final class BASE_PATH {
        public static final String DEFAULT = "/cms/user";
        public static final String CMS_PROFILE = "/cms/user/profile";
    }

	public static final class BASE_ROLE {
        public static final String CMS_USER = "cms_user";
    }

	public static final class ENTITY_TABLE_NAME {
		public static final String APPMOBILEUSERS = "app_mobile_users";
		public static final String APPCMSUSERS = "app_cms_users";
		public static final String APPCMSUSERSDRAFT = "app_cms_users_draft";
		public static final String APPMENUS = "app_menus";
		public static final String APPCATEGORIES = "app_categories";
		public static final String APPROLES = "app_roles";
		public static final String APPROLEMENU = "app_role_menu";
		public static final String APPROLEUSER = "app_role_user";

		public static final String PRMSYSTEM = "prm_system";
	}

	public static final class ENTITY_PREFIX {
		public static final String APPMOBILEUSERS = "amu_";
		public static final String APPCMSUSERS = "acu_";
		public static final String APPCMSUSERSDRAFT = "acud_";
		public static final String APPMENUS = "am_";
		public static final String APPCATEGORIES = "ac_";
		public static final String APPROLES = "ar_";
		public static final String APPROLEMENU = "arm_";
		public static final String APPROLEUSER = "aru_";
		
		public static final String PRMSYSTEM = "ps_";
	}

	public static final class ENTITY_STATUS {
		public static final String YES = "1";
		public static final String NO = "0";
	}

	public static final class RESPONSE_RESULT {
		public static final String SUCCESS = "SUCCESS";
		public static final String FAILED = "FAILED";
		public static final String EXPIRED = "EXPIRED";
	}

	public static final class EMAIL_TEMPLATE {
        public static final String BLANK = "blankTemplate";
	}

}
