package com.kaya.cms.user.config;

import java.util.HashMap;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.http.utils.Base64Coder;

import lombok.Getter;

@Configuration
@Getter
public class KeycloakProvider {

	@Value("${keycloak.auth-server-url}")
	public String serverURL;
    @Value("${keycloak.realm}")
    public String realm;
    @Value("${keycloak.resource}")
    public String clientID;
    @Value("${keycloak.credentials.secret}")
    public String clientSecret;

    private static Keycloak keycloak = null;

    public KeycloakProvider() {
    }

    public Keycloak getInstance(String username, String password) {
        if (keycloak == null) {
            keycloak = KeycloakBuilder.builder()
            							.realm(realm)
            							.serverUrl(serverURL)
            							.clientId(clientID)
            							.clientSecret(clientSecret)
            							.username(username)
            							.password(password)
            							.grantType(OAuth2Constants.PASSWORD)
            							.build();
        }

        return keycloak;
    }

    public KeycloakBuilder newKeycloakBuilderWithPasswordCredentials(String username, String password) {
        return KeycloakBuilder.builder()
                .realm(realm)
                .serverUrl(serverURL)
                .clientId(clientID)
                .clientSecret(clientSecret)
                .username(username)
                .password(password);
    }

    public HashMap<String, Object> newToken(String username, String password) {
        try {
	        String url = serverURL + "/realms/" + realm + "/protocol/openid-connect/token";
	        JsonNode resp = Unirest.post(url)
	        		.header("Content-Type", "application/x-www-form-urlencoded")
	        		.header("Authorization", "Basic " + Base64Coder.encodeString(clientID + ":" + clientSecret))
	                .field("username", username)
	                .field("password", password)
	                .field("grant_type", "password")
	                .asJson().getBody();
	        return new ObjectMapper().readValue(resp.toString(), new TypeReference<HashMap<String, Object>>() {});
		} catch (UnirestException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
        return new HashMap<String, Object>();
    }

    public HashMap<String, Object> refreshToken(String refreshToken) {
        try {
	        String url = serverURL + "/realms/" + realm + "/protocol/openid-connect/token";
	        JsonNode resp = Unirest.post(url)
	        		.header("Content-Type", "application/x-www-form-urlencoded")
	                .field("client_id", clientID)
	                .field("client_secret", clientSecret)
	                .field("refresh_token", refreshToken)
	                .field("grant_type", "refresh_token")
	                .asJson().getBody();
	        return new ObjectMapper().readValue(resp.toString(), new TypeReference<HashMap<String, Object>>() {});
		} catch (UnirestException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

        return new HashMap<String, Object>();
    }

}
