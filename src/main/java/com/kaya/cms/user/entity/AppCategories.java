package com.kaya.cms.user.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.kaya.cms.user.common.CommonConstant.ENTITY_PREFIX;
import com.kaya.cms.user.common.CommonConstant.ENTITY_TABLE_NAME;

import lombok.Data;

@Data
@Entity
@Table(name = ENTITY_TABLE_NAME.APPCATEGORIES)
public class AppCategories implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String PREFIX = ENTITY_PREFIX.APPCATEGORIES;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PREFIX + "id")
	private Integer id;

    @Column(name = PREFIX + "user_crt", length = 36)
	private String userCrt;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = PREFIX + "dtm_crt")
	private Date dtmCrt;

    @Column(name = PREFIX + "user_upd", length = 36)
	private String userUpd;

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = PREFIX + "dtm_upd")
	private Date dtmUpd;

    @Column(name = PREFIX + "is_active", length = 1)
	private String isActive;

    @Version
    @Column(name = PREFIX + "version_id", columnDefinition = "int4")
	private long versionId;

    @Column(name = PREFIX + "code", length = 20)
	private String code;

    @Column(name = PREFIX + "seq")
	private Integer seq;

    @Column(name = PREFIX + "type", length = 10)
	private String type;

    @Column(name = PREFIX + "label", length = 255)
	private String label;

    @Column(name = PREFIX + "icon", length = 255)
	private String icon;

}
