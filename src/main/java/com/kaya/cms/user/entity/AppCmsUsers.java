package com.kaya.cms.user.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.kaya.cms.user.common.CommonConstant.ENTITY_PREFIX;
import com.kaya.cms.user.common.CommonConstant.ENTITY_TABLE_NAME;

import lombok.Data;

@Data
@Entity
@Table(name = ENTITY_TABLE_NAME.APPCMSUSERS)
public class AppCmsUsers implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String PREFIX = ENTITY_PREFIX.APPCMSUSERS;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PREFIX + "id")
	private Integer id;

    @Column(name = PREFIX + "user_crt", length = 36)
	private String userCrt;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = PREFIX + "dtm_crt")
	private Date dtmCrt;

    @Column(name = PREFIX + "user_upd", length = 36)
	private String userUpd;

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = PREFIX + "dtm_upd")
	private Date dtmUpd;

    @Column(name = PREFIX + "is_active", length = 1)
	private String isActive;

    @Column(name = PREFIX + "user_id", length = 36)
	private String userId;

    @Column(name = PREFIX + "email", length = 255)
	private String email;

    @Column(name = PREFIX + "first_name", length = 255)
	private String firstName;

    @Column(name = PREFIX + "last_name", length = 255)
	private String lastName;

    @Column(name = PREFIX + "full_name", length = 500)
	private String fullName;

    @Column(name = PREFIX + "mobile_phone", length = 255)
	private String mobilePhone;

    @Column(name = PREFIX + "id_provider", length = 50)
	private String identityProvider;

    @Column(name = PREFIX + "is_expired_password", length = 1)
	private String isExpiredPassword;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = PREFIX + "dtm_expired_password")
	private Date dtmPasswordExpired;

    @Column(name = PREFIX + "is_locked_password", length = 1)
	private String isLockedPassword;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = PREFIX + "dtm_locked_password")
	private Date dtmLockedPassword;

    @Column(name = PREFIX + "cnt_failed_password")
	private Integer countFailedPassword;

    @Column(name = PREFIX + "acud_id")
	private Integer cmsUserDraftId;

}

