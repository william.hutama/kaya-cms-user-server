package com.kaya.cms.user.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.UpdateTimestamp;

import com.kaya.cms.user.common.CommonConstant.ENTITY_PREFIX;
import com.kaya.cms.user.common.CommonConstant.ENTITY_TABLE_NAME;

import lombok.Data;

@Data
@Entity
@Table(name = ENTITY_TABLE_NAME.PRMSYSTEM)
public class PrmSystem implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String PREFIX = ENTITY_PREFIX.PRMSYSTEM;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PREFIX + "id")
	private Integer id;

    @Column(name = PREFIX + "user_upd", length = 36)
	private String userUpd;

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = PREFIX + "dtm_upd")
	private Date dtmUpd;

    @Column(name = PREFIX + "is_active", length = 1)
	private String isActive;

    @Version
    @Column(name = PREFIX + "version_id", columnDefinition = "int4")
	private long versionId;

    @Column(name = PREFIX + "code", length = 20)
	private String code;

    @Column(name = PREFIX + "name", length = 255)
	private String name;

    @Column(name = PREFIX + "data_type", length = 20)
	private String dataType;

    @Column(name = PREFIX + "data_size")
	private Integer dataSize;

    @Column(name = PREFIX + "key", length = 1000)
	private String key;

    @Column(name = PREFIX + "value", length = 1000)
	private String value;
    
    @Column(name = PREFIX + "description", length = 1000)
	private String description;

}
