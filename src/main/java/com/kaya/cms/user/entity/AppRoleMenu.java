package com.kaya.cms.user.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.kaya.cms.user.common.CommonConstant.ENTITY_PREFIX;
import com.kaya.cms.user.common.CommonConstant.ENTITY_TABLE_NAME;

import lombok.Data;

@Data
@Entity
@Table(name = ENTITY_TABLE_NAME.APPROLEMENU)
public class AppRoleMenu implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String PREFIX = ENTITY_PREFIX.APPROLEMENU;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PREFIX + "id")
	private Integer id;

    @Column(name = PREFIX + "ar_id")
	private Integer roleId;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = PREFIX + "ar_id", insertable = false, updatable = false, referencedColumnName = "ar_id")
	private AppRoles role;

    @Column(name = PREFIX + "am_id")
	private Integer menuId;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = PREFIX + "am_id", insertable = false, updatable = false, referencedColumnName = "am_id")
	private AppMenus menu;
    
    @Column(name = PREFIX + "ac_id")
	private Integer categoryId;

}
