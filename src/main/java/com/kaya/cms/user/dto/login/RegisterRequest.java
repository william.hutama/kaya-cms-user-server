package com.kaya.cms.user.dto.login;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class RegisterRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private String email;
	private String password;
	private String firstName;
	private String lastName;
	private String mobilePhone;
	private List<String> resultDesc;

}
