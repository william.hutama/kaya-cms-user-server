package com.kaya.cms.user.dto.application;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class AppCmsUsersDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String userCrt;
	private Date dtmCrt;
	private String userUpd;
	private Date dtmUpd;
	private String isActive;
	private String userId;
	private String email;
	private String firstName;
	private String lastName;
	private String fullName;
	private String mobilePhone;
	private String identityProvider;
	private String isExpiredPassword;
	private Date dtmPasswordExpired;
	private String isLockedPassword;
	private Date dtmLockedPassword;
	private Integer countFailedPassword;
	private Integer cmsUserDraftId;

}
