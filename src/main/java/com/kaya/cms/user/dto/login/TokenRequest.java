package com.kaya.cms.user.dto.login;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TokenRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty(value = "refresh_token")
	private String refreshToken;

}
