package com.kaya.cms.user.dto.application;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class AppMenusDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String userCrt;
	private Date dtmCrt;
	private String userUpd;
	private Date dtmUpd;
	private String isActive;
	private long versionId;
	private Integer parentMenuId;
	private AppMenusDto parentMenu;
	private Integer categoryId;
	private AppCategoriesDto category;
	private String code;
	private Integer level;
	private Integer seq;
	private String type;
	private String label;
	private String icon;
	private String path;
	private String tag;
	private String langKey;

}
