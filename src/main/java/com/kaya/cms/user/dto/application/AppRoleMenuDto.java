package com.kaya.cms.user.dto.application;

import java.io.Serializable;

import lombok.Data;

@Data
public class AppRoleMenuDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String roleId;
	private AppRolesDto role;
	private Integer menuId;
	private AppMenusDto menu;
	private Integer categoryId;
	private String selectedMenuId;

}
