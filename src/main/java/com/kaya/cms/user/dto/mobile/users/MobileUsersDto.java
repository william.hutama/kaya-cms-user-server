package com.kaya.cms.user.dto.mobile.users;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class MobileUsersDto  implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;

    private String userCrt;

    private Date dtmCrt;

    private String userUpd;

    private Date dtmUpd;

    private String isActive;

    private String userId;

    private String email;

    private String firstName;

    private String lastName;

    private String fullName;

    private String mobilePhone;

    private String temporaryMobilePhone;

    private String appleId;

    private String appleEmail;
    
    private Date dtmLastLogin;

    private String isAppleLinked;

    private String googleId;

    private String googleEmail;

    private String isGoogleLinked;

    private String isExpiredPassword;

    private Date dtmPasswordExpired;

    private String isLockedPassword;

    private Date dtmLockedPassword;

    private Integer countFailedPassword;

    private String isInitialPin;

    private String pin;

    private String temporaryPin;
    
    private String deviceId;
    
    private String fingerprintToggle;
    
    private String fingerprintHashKey;

    private Integer mobileUserDraftId;
}
