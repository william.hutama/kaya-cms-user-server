package com.kaya.cms.user.dto.login;

import java.io.Serializable;

import lombok.Data;

@Data
public class LoginRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private String email;
	private String password;

}
