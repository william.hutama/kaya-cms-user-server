package com.kaya.cms.user.dto.profile;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProfileResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private String email;
	private String firstName;
	private String lastName;
	private String fullName;
	private String mobilePhone;

}
