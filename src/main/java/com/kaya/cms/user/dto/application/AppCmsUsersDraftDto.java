package com.kaya.cms.user.dto.application;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class AppCmsUsersDraftDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Date dtmCrt;
	private Date dtmUpd;
	private String isActive;
	private String email;
	private String firstName;
	private String lastName;
	private String mobilePhone;
	private String identityProvider;
	private String password;

}
