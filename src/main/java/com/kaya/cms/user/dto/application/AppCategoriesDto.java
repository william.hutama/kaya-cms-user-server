package com.kaya.cms.user.dto.application;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class AppCategoriesDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String userCrt;
	private Date dtmCrt;
	private String userUpd;
	private Date dtmUpd;
	private String isActive;
	private long versionId;
	private String code;
	private Integer seq;
	private String type;
	private String label;
	private String icon;

}
