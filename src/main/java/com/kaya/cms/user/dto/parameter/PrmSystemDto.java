package com.kaya.cms.user.dto.parameter;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class PrmSystemDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String userUpd;
	private Date dtmUpd;
	private String isActive;
	private long versionId;
	private String code;
	private String name;
	private String dataType;
	private Integer dataSize;
	private String key;
	private String value;
	private String description;

}
