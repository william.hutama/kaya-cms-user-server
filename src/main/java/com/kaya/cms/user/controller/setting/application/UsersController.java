package com.kaya.cms.user.controller.setting.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kaya.cms.user.common.CommonConstant.BASE_PATH;
import com.kaya.cms.user.service.AppCmsUsersService;

@RestController
@RequestMapping(BASE_PATH.DEFAULT + "/setting/application/users")
public class UsersController {

	@Autowired
	AppCmsUsersService appCmsUsersService;

}
