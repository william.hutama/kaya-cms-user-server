package com.kaya.cms.user.controller.setting.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kaya.cms.user.base.BaseController;
import com.kaya.cms.user.common.CommonConstant.BASE_PATH;
import com.kaya.cms.user.dto.application.AppMenusDto;
import com.kaya.cms.user.dto.application.AppRoleMenuDto;
import com.kaya.cms.user.entity.AppMenus;
import com.kaya.cms.user.entity.AppRoleMenu;
import com.kaya.cms.user.service.AppRoleMenuService;


@RestController
@RequestMapping(BASE_PATH.DEFAULT + "/setting/application/rolemenu")
public class RoleMenuController {

	@Autowired
	AppRoleMenuService appRoleMenuService;

	@GetMapping("/active")
	@ResponseBody
	public HttpEntity<Map<String,Object>> getActiveRoleMenu(HttpServletRequest request, @RequestParam Map<String, String> searchParams) {
		List<AppRoleMenu> armList = appRoleMenuService.findAllByRoleIdAndCategoryIdAndMenuIsActive(1, new Integer(searchParams.get("categoryId").toString()));

		List<AppMenus> armListTemp = new ArrayList<AppMenus>();

		for(AppRoleMenu arm : armList) {
			armListTemp.add(arm.getMenu());
		}

		return BaseController.getResponseEntityList(armListTemp, AppMenusDto.class, request, HttpStatus.OK, "", null);
	}

	@PostMapping("/update")
	@ResponseBody
	public HttpEntity<Map<String,Object>> update(HttpServletRequest request, @RequestBody AppRoleMenuDto data) {
		String updateRoleMenuResult = appRoleMenuService.updateRoleMenu(data.getCategoryId(), data.getRoleId(), data.getSelectedMenuId());
		return BaseController.getResponseEntity(null, null, request, HttpStatus.OK, "success", null);
	}

}
