package com.kaya.cms.user.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kaya.cms.user.base.BaseController;
import com.kaya.cms.user.common.CommonConstant.BASE_PATH;
import com.kaya.cms.user.dto.profile.ProfileResponse;
import com.kaya.cms.user.entity.AppCmsUsers;
import com.kaya.cms.user.service.AppCmsUsersService;

@RestController
@RequestMapping(BASE_PATH.CMS_PROFILE)
public class MyProfileController {

	@Autowired
	private AppCmsUsersService appCmsUsersService;

	@GetMapping
	public HttpEntity<Map<String, Object>> getProfile(
			@RequestHeader(value = "requestId", required = false) String requestId,
			Authentication auth, HttpServletRequest httpServletRequest) {

		String userId = auth!=null ? auth.getPrincipal().toString() : null;
		AppCmsUsers amu = appCmsUsersService.getProfile(userId);

		ProfileResponse response = new ProfileResponse();
		if (amu!=null) {
			response.setEmail(amu.getEmail());
			response.setFirstName(amu.getFirstName());
			response.setLastName(amu.getLastName());
			response.setFullName(amu.getFirstName() + " " + amu.getLastName());
			response.setMobilePhone(amu.getMobilePhone());

	        return BaseController.getResponseEntity(response, httpServletRequest, HttpStatus.OK, "", requestId);
		}

        return BaseController.getResponseEntity(response, httpServletRequest, HttpStatus.BAD_REQUEST, "", requestId);
	}

}
