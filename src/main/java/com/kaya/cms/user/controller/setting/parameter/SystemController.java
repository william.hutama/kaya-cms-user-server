package com.kaya.cms.user.controller.setting.parameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kaya.cms.user.base.BaseControllerHelper;
import com.kaya.cms.user.base.BaseService;
import com.kaya.cms.user.common.CommonConstant.BASE_PATH;
import com.kaya.cms.user.dto.parameter.PrmSystemDto;
import com.kaya.cms.user.entity.PrmSystem;
import com.kaya.cms.user.service.PrmSystemService;

@RestController
@RequestMapping(BASE_PATH.DEFAULT + "/setting/parameter/system")
public class SystemController extends BaseControllerHelper<PrmSystem, PrmSystemDto, Integer> {

	@Autowired
	PrmSystemService prmSystemService;

	@Override
	public BaseService<PrmSystem, Integer> getBaseService() {
		return prmSystemService;
	}

}
