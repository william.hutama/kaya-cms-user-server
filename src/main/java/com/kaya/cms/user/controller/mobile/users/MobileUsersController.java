package com.kaya.cms.user.controller.mobile.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kaya.cms.user.base.BaseControllerHelper;
import com.kaya.cms.user.base.BaseService;
import com.kaya.cms.user.common.CommonConstant.BASE_PATH;
import com.kaya.cms.user.dto.mobile.users.MobileUsersDto;
import com.kaya.cms.user.entity.AppMobileUsers;
import com.kaya.cms.user.service.AppMobileUsersService;

@RestController
@RequestMapping(BASE_PATH.DEFAULT + "/setting/mobile/user")
public class MobileUsersController extends BaseControllerHelper<AppMobileUsers, MobileUsersDto, Integer>{

	@Autowired
	AppMobileUsersService appMobileUsersService;
	
	@Override
	public BaseService<AppMobileUsers, Integer> getBaseService() {
		return appMobileUsersService;
	}
}
