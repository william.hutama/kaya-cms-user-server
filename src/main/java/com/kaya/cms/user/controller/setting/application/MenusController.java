package com.kaya.cms.user.controller.setting.application;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kaya.cms.user.base.BaseController;
import com.kaya.cms.user.common.CommonConstant.BASE_PATH;
import com.kaya.cms.user.dto.application.AppMenusDto;
import com.kaya.cms.user.entity.AppMenus;
import com.kaya.cms.user.service.AppMenusService;

@RestController
@RequestMapping(BASE_PATH.DEFAULT + "/setting/application/menus")
public class MenusController  {

	@Autowired
	AppMenusService appMenusService;

	@GetMapping("/category/type/{categoryType}")
	@ResponseBody
	public HttpEntity<Map<String,Object>> getAllByCategoryType(@RequestHeader(value = "requestId", required = false) String requestId,
															   @PathVariable("categoryType") String categoryType,
															   Pageable pageable,
															   HttpServletRequest request) {
        Page<AppMenus> data = appMenusService.findAllByCategoryType(categoryType, pageable);
        return BaseController.getResponseEntityPage(data, AppMenusDto.class, request, HttpStatus.OK, "", requestId);
	}

	@GetMapping("/category/type/{categoryType}/{isActive}")
	@ResponseBody
	public HttpEntity<Map<String,Object>> getAllByCategoryTypeAndIsActive(@RequestHeader(value = "requestId", required = false) String requestId,
																		  @PathVariable("categoryType") String categoryType,
																		  @PathVariable("isActive") String isActive,
																		  Pageable pageable,
																		  HttpServletRequest request) {
        Page<AppMenus> data = appMenusService.findAllByCategoryTypeAndIsActive(categoryType, isActive, pageable);
        return BaseController.getResponseEntityPage(data, AppMenusDto.class, request, HttpStatus.OK, "", requestId);
	}

}
