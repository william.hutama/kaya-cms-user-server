package com.kaya.cms.user.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kaya.cms.user.common.CommonConstant.BASE_PATH;
import com.kaya.cms.user.dto.login.LoginRequest;
import com.kaya.cms.user.dto.login.TokenRequest;
import com.kaya.cms.user.service.AppCmsUsersService;
import com.kaya.cms.user.service.KeycloakAdminClientService;

@RestController
@RequestMapping(BASE_PATH.DEFAULT)
public class LoginController {

	@Autowired
	private Environment env;
	
	@Autowired
    private KeycloakAdminClientService kcAdminClient;
	
	@Autowired
	AppCmsUsersService appCmsUsersService;

	@GetMapping
	public String info() {
		return "CMS User Service running at port: " + env.getProperty("local.server.port");
	}
	
	@PostMapping("/login")
	@ResponseBody
	public HashMap<String, Object> login(
			@RequestHeader(value = "requestId", required = false) String requestId,
			HttpServletResponse httpServletResponse, @RequestBody LoginRequest request) {

		HashMap<String, Object> response = kcAdminClient.newToken(request.getEmail(), request.getPassword());
		if (response!=null) {
			if (response.get("error")!=null) {
				httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
			}
		}

        return response;
	}

	@PostMapping("/refresh")
	@ResponseBody
	public HashMap<String, Object> refresh(
			@RequestHeader(value = "requestId", required = false) String requestId,
			HttpServletResponse httpServletResponse, @RequestBody TokenRequest request) {

		HashMap<String, Object> response = kcAdminClient.refreshToken(request.getRefreshToken());
		if (response!=null) {
			if (response.get("error")!=null) {
				httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
			}
		}

        return response;
	}

}
